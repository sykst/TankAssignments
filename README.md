# TankAssignments 2.0 - by Atreyyo (modified by Sykst)

A World of Warcraft 1.12 AddOn that helps you assign tanks to raidtargets


| Command | Description |
| --- | --- |
| /ta  | open/close TankAssignments |
| /ta colors | turn on/off chatcolors |
